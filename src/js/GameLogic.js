import Animations from "./Animations";
import Light from "./Light";

export default function GameLogic(THREE, drawing, game, net) {
    const animations = new Animations();
    let currentCard = null;

    const raycaster = new THREE.Raycaster();
    let mouseVector = new THREE.Vector2();

    let firstCard = null;
    let secondCard = null;

    const light = new Light(THREE);
    drawing.scene.add(light.getLight());

    this.showPrompt = () => {
        if (firstCard.userData.image.split(".")[0] != "0") {
            let imageNumber = firstCard.userData.image.split(".")[0];
            firstCard.userData.image.split(".")[1] == "1" ? imageNumber += ".2" : imageNumber += ".1";
            let img = document.createElement("img");
            img.src = `./gfx/${imageNumber}.png`;
            img.id = "promptImg";
            document.getElementById("promptBox").appendChild(img);
        }
    }

    this.hidePrompt = () => {
        if (firstCard.userData.image.split(".")[0] != "0") {
            document.getElementById("promptImg").remove();
        }
    }

    const clearCards = () => {
        setTimeout(function () {
            animations.rotate(secondCard);
            animations.rotate(firstCard);
            currentCard = null;
            firstCard = null;
            secondCard = null;
            for (let card of drawing.cardsArray) {
                card.getCard().position.y = 1;
            }
        }, 1000);
    }

    // highlight cards
    document.addEventListener("mousemove", (e) => {
        if (game.move) {
            mouseVector.x = (e.clientX / window.innerWidth) * 2 - 1;
            mouseVector.y = -(e.clientY / window.innerHeight) * 2 + 1;
            raycaster.setFromCamera(mouseVector, drawing.camera);
            let intersects = raycaster.intersectObjects(drawing.scene.children);
            // move up highlighted card
            if (intersects.length > 0 && intersects[0].object != currentCard) {
                currentCard ? animations.moveDown(currentCard) : null;
                currentCard = intersects[0].object;
                animations.moveUp(currentCard);
                // move down highlighted card
            } else if (intersects.length === 0 && currentCard != null) {
                animations.moveDown(currentCard);
                currentCard = null;
            }
        }
    });

    // select cards
    document.addEventListener("click", (e) => {
        if (game.move) {
            mouseVector.x = (e.clientX / window.innerWidth) * 2 - 1;
            mouseVector.y = -(e.clientY / window.innerHeight) * 2 + 1;
            raycaster.setFromCamera(mouseVector, drawing.camera);
            let intersects = raycaster.intersectObjects(drawing.scene.children);
            // select first card
            if (intersects.length > 0 && firstCard == null) {
                firstCard = intersects[0].object;
                animations.rotate(intersects[0].object);
                net.makeMove({ type: "first", card: intersects[0].object.userData.image });
                this.showPrompt();
                // select second card
            } else if (intersects.length > 0 && firstCard != null && firstCard != intersects[0].object && secondCard == null) {
                secondCard = intersects[0].object;
                animations.rotate(intersects[0].object);
                this.hidePrompt();

                // check if the cards form a pair
                if (game.check(firstCard, secondCard)) {
                    game.userPoints++;
                    setTimeout(function () {
                        light.setGreen();
                        drawing.scene.remove(firstCard);
                        drawing.scene.remove(secondCard);
                    }, 1000);
                    game.pairsLeft--;
                    clearCards();
                } else {
                    setTimeout(() => {
                        light.setRed();
                    }, 500)
                    clearCards();
                }
                net.makeMove({ type: "second", card: intersects[0].object.userData.image });
            }
        }
    });
}
import Net from "./Net";

export function UiConnections(net) {
    let helper = false;
    let tutorial = false;
    let history = false;
    let prompt = false;

    // connect to game 
    document.getElementById("connect").addEventListener("click", function () {
        if (document.getElementById("nick").value != "") {
            net.connect();
            net.logIn(document.getElementById("nick").value);
            document.getElementById("logBox").style.display = "none";
        } else {
            alert("Wprowadź nick");
        }
    });

    // show help 
    document.getElementById("showHelper").addEventListener("click", function () {
        if (!helper) {
            document.getElementById("helpBox").style.display = "block";
            helper = true;
        } else {
            document.getElementById("helpBox").style.display = "none";
            helper = false;
        }
    });

    // show tutorial 
    document.getElementById("showTutorial").addEventListener("click", function () {
        if (!tutorial) {
            document.getElementById("tutorialBox").style.display = "block";
            tutorial = true;
        } else {
            document.getElementById("tutorialBox").style.display = "none";
            tutorial = false;
        }
    });

    // show history 
    document.getElementById("showHistory").addEventListener("click", function () {
        if (!history) {
            document.getElementById("historyBox").style.display = "block";
            history = true;
        } else {
            document.getElementById("historyBox").style.display = "none";
            history = false;
        }
    });
    
    // show prompt
    document.getElementById("showPrompt").addEventListener("click", function () {
        if (!prompt) {
            document.getElementById("promptBox").style.display = "block";
            prompt = true;
        } else {
            document.getElementById("promptBox").style.display = "none";
            prompt = false;
        }
    });

    // create tutorial table with pairs
    let table = document.createElement("table");
    table.id = "pictures";
    for (let i = 0; i < 18; i++) {
        let tr = document.createElement("tr");
        for (let j = 1; j <= 2; j++) {
            let td = document.createElement("td");
            let img = document.createElement("img");
            img.src = `gfx/${i}.${j}.png`;
            td.appendChild(img);
            tr.appendChild(td);
        }
        table.appendChild(tr);
    }
    document.getElementById("tutorialBox").innerHTML += "SPIS PAR:"
    document.getElementById("tutorialBox").appendChild(table);
}

export function UiMessage() {
    let messageBox = document.getElementById("infoBox");
    let message = document.getElementById("info");

    this.showMessage = (text, color) => {
        messageBox.style.display = "block";
        message.innerHTML = text;
        message.style.color = color;
    }

    this.hideMessage = () => {
        messageBox.style.display = "none";
    }
}
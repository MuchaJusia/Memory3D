export default function Card(THREE, image) {
    let materials = [];

    materials.push(new THREE.MeshPhongMaterial({ side: THREE.DoubleSide, color: 0x2e2f34 }));
    materials.push(new THREE.MeshPhongMaterial({ side: THREE.DoubleSide, color: 0x2e2f34 }));
    materials.push(new THREE.MeshPhongMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load(`../gfx/${image}.png`), rotation: Math.PI / 2 }));
    materials.push(new THREE.MeshPhongMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load(`../gfx/back.jpg`) }));
    materials.push(new THREE.MeshPhongMaterial({ side: THREE.DoubleSide, color: 0x2e2f34 }));
    materials.push(new THREE.MeshPhongMaterial({ side: THREE.DoubleSide, color: 0x2e2f34 }));

    let geometry = new THREE.BoxGeometry(75, 2, 71);
    let mesh = new THREE.Mesh(geometry, materials);


    this.getCard = () => {
        return mesh;
    }
}
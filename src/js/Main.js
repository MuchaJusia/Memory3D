require('../scss/style.scss');
let THREE = require('three');

import Game from './Game';
import Drawing from './Drawing';
import Net from './Net';
import GameLogic from './GameLogic';
import { UiConnections } from './Ui';
import Helper from './Helper';
import HelperModel from './HelperModel';

const helperModel = new HelperModel(THREE);
const game = new Game(helperModel);
const drawing = new Drawing(THREE, game);
const net = new Net(game, drawing, helperModel);
const uiConnections = new UiConnections(net);
const gameLogic = new GameLogic(THREE, drawing, game, net);
const helper = new Helper(THREE, helperModel);
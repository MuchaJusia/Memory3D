import Card from './Card';
import Helper from './Helper';

export default function Drawing(THREE, game, gameLogic) {
    let renderer;
    this.scene = null;
    this.camera = null;

    this.cardsArray = [];

    // create scene and camera
    (() => {
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.camera.position.set(0, 100, 10);
        this.camera.lookAt(this.scene.position);
        this.camera.position.set(200, 250, 240);
        renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setClearColor(0xffffff);
        renderer.setSize(window.innerWidth, window.innerHeight);
        document.body.appendChild(renderer.domElement);
    })();

    // add light to scene
    

    const render = () => {
        renderer.render(this.scene, this.camera);
        requestAnimationFrame(render);
    }

    // add cards to scene
    this.addCards = (array) => {
        let z = 0;
        let x = 0;
        for (let i = 0; i < game.arraySize; i++) {
            for (let j = 0; j < game.arraySize; j++) {
                let card = new Card(THREE, array[i][j]);
                card.getCard().userData = { image: array[i][j] }
                this.scene.add(card.getCard());
                card.getCard().position.set(x, 1, z);
                card.getCard().rotateX(Math.PI);
                card.getCard().rotateY(Math.PI);
                this.cardsArray.push(card);
                z += 76;
            }
            z = 0;
            x += 80;
        }
    }

    render();
}
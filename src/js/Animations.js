export default function Animations() {
    this.moveUp = (card) => {
        let i = 0;
        const move = () => {
            card.position.y += .2;
            i++;
            i == 30 ? null : requestAnimationFrame(move);
        }
        move();
    }

    this.moveDown = (card) => {
        let i = 0;
        const move = () => {
            card.position.y -= .2;
            i++;
            i == 30 ? null : requestAnimationFrame(move);
        }
        move();
    }

    this.rotate = (card) => {
        let i = 0;
        const move = () => {
            card.rotateZ(Math.PI / 30);
            i++;
            i == 30 ? null : requestAnimationFrame(move);
        }
        move();
    }
}
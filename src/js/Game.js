export default function Game(helperModer) {
    this.userPoints = 0;
    this.oponnentPoints = 0;
    this.userName = null;
    this.oponnentName = null;
    this.move = false;
    this.pairsLeft = 18;
    this.arraySize = 6;
    this.imageArray = [
        ["0.1", "0.2", "1.1", "1.2", "2.1", "2.2"],
        ["3.1", "3.2", "4.1", "4.2", "5.1", "5.2"],
        ["6.1", "6.2", "7.1", "7.2", "8.1", "8.2"],
        ["9.1", "9.2", "10.1", "10.2", "11.1", "11.2"],
        ["12.1", "12.2", "13.1", "13.2", "14.1", "14.2"],
        ["15.1", "15.2", "16.1", "16.2", "17.1", "17.2"]
    ]

    this.checkWin = () => {
        if (this.pairsLeft == 1) {
            if (this.oponnentPoints > this.userPoints) {
                helperModer.setInfo("Wygrywa przeciwnik.", this.oponnentPoints, this.userPoints, this.oponnentName, this.userName);
            } else if (this.userPoints > this.oponnentPoints) {
                helperModer.setInfo("Wygrywasz grę.", this.oponnentPoints, this.userPoints, this.oponnentName, this.userName);
            } else if (this.oponnentPoints == this.userPoints) {
                helperModer.setInfo("Remis.");
            }
            document.getElementById("helpBox").style.display = "block";
            return true;
        } else {
            return false;
        }
    }

    this.check = (firstCard, secondCard) => {
        if (firstCard.userData.image.split(".")[0] == secondCard.userData.image.split(".")[0] && secondCard.userData.image.split(".")[0] != "0") {
            return true;
        } else {
            return false;
        }
    }

    this.shuffleArray = () => {
        for (let i = 0; i < this.arraySize; i++) {
            for (let j = 0; j < this.arraySize; j++) {
                const index1 = ~~(Math.random() * 6);
                const index2 = ~~(Math.random() * 6);
                const temp = this.imageArray[i][j];
                this.imageArray[i][j] = this.imageArray[index1][index2];
                this.imageArray[index1][index2] = temp;
            }
        }
    }
}
import { UiMessage } from "./Ui";
import Animations from "./Animations";

export default function Net(game, drawing, helperModel) {
    const uiMessage = new UiMessage();
    const animations = new Animations();

    let firstCard = null;
    let secondCard = null;

    let client;
    this.connect = () => {
        client = io();

        client.on("onconnect", function (data) {
            // first player
            if (data.start) {
                game.shuffleArray();
                drawing.addCards(game.imageArray);
                uiMessage.showMessage("Oczekiwanie na gracza", "green");
                client.emit("sendArray", { array: JSON.stringify(game.imageArray) });
            } else {
                // second player
                if (data.array) {
                    game.oponnentName = data.nick;
                    game.imageArray = JSON.parse(data.array);
                    drawing.addCards(game.imageArray);
                    uiMessage.showMessage("Dołączono do gry.", "blue");
                    setTimeout(function () {
                        uiMessage.showMessage("Przeciwnik zaczyna grę.", "blue");
                    }, 1000);
                    setTimeout(function () {
                        uiMessage.hideMessage();
                    }, 2000);
                    // other players
                } else {
                    alert("gra zajęta");
                }
            }

            // generate history table form database info
            (() => {
                let table = document.createElement("table");
                let tr = document.createElement("tr");
                let td1 = document.createElement("td");
                let td2 = document.createElement("td");
                let td3 = document.createElement("td");
                td1.innerHTML = "DATA";
                td2.innerHTML = "GRACZ 1";
                td3.innerHTML = "GRACZ 2";
                tr.appendChild(td1);
                tr.appendChild(td2);
                tr.appendChild(td3);
                table.appendChild(tr);
                for (let history of JSON.parse(data.history)) {
                    let tr = document.createElement("tr");
                    let td1 = document.createElement("td");
                    let td2 = document.createElement("td");
                    let td3 = document.createElement("td");
                    td1.innerHTML = history.date;
                    td2.innerHTML = history.user1;
                    td3.innerHTML = history.user2;
                    tr.appendChild(td1);
                    tr.appendChild(td2);
                    tr.appendChild(td3);
                    table.appendChild(tr);
                }
                document.getElementById("historyBox").appendChild(table);
                document.getElementById("showHistory").removeAttribute("disabled");
            })();
        });

        client.on("oponnentConnect", function (data) {
            game.oponnentName = data.nick;

            uiMessage.showMessage("Przeciwnik dołączył do gry.", "blue");

            setTimeout(function () {
                uiMessage.showMessage("Zaczynasz grę.", "blue");
            }, 1000);

            setTimeout(function () {
                uiMessage.hideMessage();
                game.move = true;
            }, 2000);
        });

        client.on("oponnentDisconnect", function () {
            uiMessage.showMessage("Przeciwnik wyszedł z gry", "red");

            setTimeout(function () {
                window.location.reload();
            }, 2000);
        });

        client.on("makeMove", function (data) {
            // rotate and select opponent card
            for (let card of drawing.cardsArray) {
                if (card.getCard().userData.image == data.image) {
                    animations.rotate(card.getCard());

                    if (data.type == "first")
                        firstCard = card.getCard();
                    else if (data.type == "second")
                        secondCard = card.getCard();
                }
            }

            // check if two cards form a pair
            if (data.type == "second") {
                game.move = true;

                // delete pair and add points
                if (game.check(firstCard, secondCard)) {
                    setTimeout(function () {
                        drawing.scene.remove(firstCard);
                        drawing.scene.remove(secondCard);
                    }, 1000);
                    game.pairsLeft--;
                    game.oponnentPoints++;
                    // reverse cards
                } else {
                    setTimeout(function () {
                        animations.rotate(secondCard);
                        animations.rotate(firstCard);
                    }, 1000);
                }

                // clear card position
                for (let card of drawing.cardsArray) {
                    card.getCard().position.y = 1;
                }

                helperModel.setInfo("Twój ruch.", game.oponnentPoints, game.userPoints, game.oponnentName, game.userName);
                game.checkWin();
            }
        });
    }

    this.logIn = (nick) => {
        client.emit("logIn", { nick: nick })
        game.userName = nick;
    }

    this.makeMove = (data) => {
        if (data.type == "first") {
            client.emit("makeMove", { type: data.type, image: data.card });
        } else if (data.type == "second") {
            client.emit("makeMove", { type: data.type, image: data.card });
            game.move = false;
            helperModel.setInfo("Ruch przeciwnika.", game.oponnentPoints, game.userPoints, game.oponnentName, game.userName);
            game.checkWin();
        }
    }
}
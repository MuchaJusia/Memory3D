export default function Helper(THREE, helperModel) {
    let scene, renderer, camera;

    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.set(0, 0, 25);
    camera.lookAt(scene.position);
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setClearColor(0x222222);
    renderer.setSize(500, 500);
    document.getElementById("helper").appendChild(renderer.domElement);

    scene.add(helperModel.getModel());

    const render = () => {
        helperModel ? helperModel.update() : null;
        renderer.render(scene, camera);
        requestAnimationFrame(render);
    }

    render();
}
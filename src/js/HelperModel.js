export default function HelperModel(THREE) {
    let container = new THREE.Object3D();
    let meshModel = null;
    let mixer = null;
    let clock = new THREE.Clock();
    let delta = clock.getDelta();

    var modelMaterial = new THREE.MeshBasicMaterial({
        map: THREE.ImageUtils.loadTexture("models/yoshi.png"),
        morphTargets: true
    });

    let loader = new THREE.JSONLoader();
    loader.load('../models/yoshi.js', function (geometry) {
        meshModel = new THREE.Mesh(geometry, modelMaterial)
        meshModel.name = "yoshi";
        meshModel.scale.set(.5, .5, .5);
        meshModel.rotation.y = Math.PI * 0.5;
        container.add(meshModel);
        mixer = new THREE.AnimationMixer(meshModel);
        mixer.clipAction("stand").play();
    });

    this.update = () => {
        delta = clock.getDelta();
        mixer ? mixer.update(delta) : null;
    }

    this.setAnimation = (animation) => {
        mixer ? mixer.clipAction(animation).play() : null;
    }

    this.getModel = () => {
        return container;
    }

    this.setInfo = (text, oponnentPoints, userPoints, oponnentNick, userNick) => {
        document.getElementById("helpText").innerHTML = text;
        document.getElementById("helpPoints").innerHTML = `${userNick}: ${userPoints} ${oponnentNick}: ${oponnentPoints}`;
    }
}
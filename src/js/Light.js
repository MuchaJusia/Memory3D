export default function Light(THREE) {
    let directionalLight = new THREE.DirectionalLight(0xffffff, 1);

    this.setGreen = function () {
        directionalLight.color.set(0x6ef442)
        setTimeout(function () { directionalLight.color.set(0xffffff); }, 500);
    }


    this.setRed = function () {
        directionalLight.color.set(0xff2d34)
        setTimeout(function () { directionalLight.color.set(0xffffff); }, 500);
    }

    this.getLight = () => {
        return directionalLight;
    }
}
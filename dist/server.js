const express = require("express");
const app = express();
const http = require("http");
const PORT = 3000;

const mongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const Operations = require("./modules/Operations.js");
let _db, _coll;
const opers = new Operations();

const socketio = require("socket.io");
const path = require("path");
app.use(express.static(__dirname + "/static"));

let usersAmount = 0;
let cardsArray = [];
let firstUser = null;
let secondUser = null;

app.get("/", function (req, res) {
    res.sendFile(path.join(__dirname + "/static/index.html"));
});

let server = app.listen(PORT, function () {
    console.log("start serwera na porcie " + PORT);
})

let io = socketio.listen(server);

io.sockets.on("connection", function (client) {
    console.log("klient się podłączył, ilość graczy: " + (usersAmount + 1));

    // connect with MongoDB
    mongoClient.connect("mongodb://localhost/history", function (err, db) {
        if (err) console.log(err)
        else console.log("mongo podłączone")
        _db = db;

        // create collection
        db.createCollection("list", function (err, coll) {
            _coll = coll;

            // select all documents (history) from database
            opers.SelectAll(_coll, function (data) {
                data = JSON.stringify(data);
                if (usersAmount === 0) {
                    client.emit("onconnect", { start: true, history: data });
                    usersAmount++;
                } else if (usersAmount === 1) {
                    usersAmount++;
                    client.emit("onconnect", { start: false, array: JSON.stringify(cardsArray), nick: firstUser, history: data });
                } else if (usersAmount > 1) {
                    usersAmount++;
                    client.emit("onconnect", { start: false, history: data });
                }
            });
        });
    });

    client.on("sendArray", function (data) {
        cardsArray = JSON.parse(data.array);
    });

    client.on("makeMove", function (data) {
        client.broadcast.emit("makeMove", { type: data.type, image: data.image });
    });

    client.on("logIn", function (data) {
        if (usersAmount == 1) {
            firstUser = data.nick;
        } else if (usersAmount == 2) {
            secondUser = data.nick;
            client.broadcast.emit("oponnentConnect", { confirm: true, nick: secondUser });

            // add history log to database
            var utc = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
            opers.Insert(_coll, { date: `${new Date().getDate()}-${new Date().getMonth()}-${new Date().getFullYear()} ${new Date().getHours()}:${new Date().getMinutes()}`, user1: firstUser, user2: secondUser });
        }
        console.log(firstUser, secondUser);
    });

    client.on("disconnect", function () {
        if (usersAmount == 2) {
            client.broadcast.emit("oponnentDisconnect", { confirm: true })
            cardsArray = [];
            usersAmount = 0;
        } else if (usersAmount == 1 && cardsArray.length == 0) {
            usersAmount--;
        } else if (usersAmount > 1 || (usersAmount == 1 && cardsArray.length > 0)) {
            usersAmount--;
            cardsArray = [];
        }
        console.log("disconnect, liczba graczy: " + usersAmount);
    });
});
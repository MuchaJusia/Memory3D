module.exports = function () {
    var opers = {
        Insert: function (collection, data, callback) {
            collection.insert(data, function (err, result) {
                //callback("dodano");
            });
        },
        SelectAll: function (collection, callback) {
            collection.find({}).toArray(function (err, items) {
                callback(items);
            });
        },
        SelectAndLimit: function (ObjectID, collection, id, callback) {
            collection.find({ _id: ObjectID(id) }).toArray(function (err, items) {
                callback(items)
            });
        },
        DeleteById: function (ObjectID, collection, id, callback) {
            collection.remove({ _id: ObjectID(id) }, function (err, data) {
                callback();
            })
        },
        UpdateById: function (ObjectID, collection, data, id, callback) {
            collection.updateOne(
                { _id: ObjectID(id) },
                { $set: data },
                function (err, data) {
                    callback();
                })
        },
        SelectDatabases: function (db, callback) {
            db.admin().listDatabases(function (err, dbs) {
                if (err) console.log(err)
                else {
                    callback(dbs.databases);
                }
            })
        },
        ShowCollections: function (db, callback) {
            db.listCollections().toArray(function (err, collInfos) {
                if (err) console.log(err);
                else {
                    callback(collInfos);
                }
            });
        },
        DeleteCollection: function (db, coll, callback) {
            db.collection(coll).drop(function (err) {
                if (err) console.log(err)
                else callback();
            });
        }

    }

    return opers;

}